#!/bin/bash

DIRS=$(echo */)
for dir in $DIRS; do
    runtime=$(echo $dir | sed 's#/##g')
    if [[ ! $runtime =~ "results"|"deploy" ]]; then
        res=$(./run.sh $runtime | grep -i report)
        echo "${runtime}: ${res}"
    fi
done
