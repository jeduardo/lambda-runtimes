package dev.jeduardo.lambda;

public class EventResponse {
  private String message;
  public void setMessage(String msg) {
    this.message = msg;
  }
  public String getMessage() {
    return this.message;
  }
  public EventResponse() {
  }
}
