package dev.jeduardo.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

public class Handler implements RequestHandler<Map<String,String>, EventResponse>{
  Gson gson = new GsonBuilder().setPrettyPrinting().create();
  @Override
  public EventResponse handleRequest(Map<String,String> event, Context context) {
    LambdaLogger logger = context.getLogger();
    logger.log("Context: " + gson.toJson(context));
    logger.log("Event: " + gson.toJson(event));
    EventResponse response = new EventResponse();
    response.setMessage("Hello from Java Lambda");
    return response;
  }
}
