using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Lambda
{

    public class InputEvent {
      public string? key { get; set; }
    }

    public class ResponseEvent {
      public string? message { get; set; }
    }
    public class Function
    {

        public ResponseEvent FunctionHandler(InputEvent ev, ILambdaContext context)
        {
            Console.WriteLine("Event", ev);
            Console.WriteLine("Context", context);
            ResponseEvent response = new();
            response.message = "Hello from dotnet lambda!";
            return response;
        }
    }
}