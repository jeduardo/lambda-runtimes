# dotnet lambda

## Workspace setup

```shell
dotnet new tool-manifest
dotnet tool install Amazon.Lambda.Tools
```

## Deployment

Create a deployment package:

```shell
dotnet lambda package dotnet-function
```

The package will be stored under `dotnet-function.zip`