#!/bin/bash

runtime=$1
if [ "x$runtime" == "x" ]; then
  runtime=python
fi

aws lambda \
  invoke \
  --function-name $runtime-lambda \
  --payload '{"key": "value"}'\
  --log-type Tail \
  --query 'LogResult' \
  --output text \
  response.json | base64 -d 

cat response.json | jq .
rm response.json
