# Sample Ruby Function

def handler(event:, context:)
    puts "Event", event
    puts "Context", context
    {
        message: "Hello from Ruby Lambda"
    }
end
