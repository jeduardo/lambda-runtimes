"""
Lambda function using Python
"""


def lambda_handler(event, context):
    """ This handler implementation returns a simple JSON payload """
    print("Event", event)
    print("Context", context)
    return {
        "message": "Hello from Python Lambda"
    }
