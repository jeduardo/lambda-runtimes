# Lambda Runtimes Comparison

Deploying lambdas with all possible runtimes so I can see differences not only
in code but also in the warmup times.

# Deploying

```shell
make package
cd deploy/terraform/
terraform apply -auto-confirm
```

# Executing

This will run a single invoke call for the specified runtime. If nothing is specified, it will assume `python` by default.

```shell
./run.sh <runtime-name>
```

# Timing

This will run an invoke call for all runtimes.

```shell
./timing.sh
```

# References

* [How to manage AWS Lambda Functions with Terraform](https://spacelift.io/blog/terraform-aws-lambda)
* [How to manage Lambda log groups with Terraform](https://advancedweb.hu/how-to-manage-lambda-log-groups-with-terraform/)
* [Run make in each subdirectory](https://stackoverflow.com/questions/17834582/run-make-in-each-subdirectory)
* [Consuming and decoding JSON in Terraform](https://dev.to/lucassha/consuming-and-decoding-json-in-terraform-309p)
* [Optimizing AWS Lambda Function performance for Java](https://aws.amazon.com/blogs/compute/optimizing-aws-lambda-function-performance-for-java/)
* [How to exclude a particular file with GNUMake's wildcard function?](https://stackoverflow.com/questions/44877381/how-to-exclude-a-particular-file-with-gnumakes-wildcard-function)
* [How to trigger Terraform to upload new lambda code?](https://stackoverflow.com/questions/48577727/how-to-trigger-terraform-to-upload-new-lambda-code)

