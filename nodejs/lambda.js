// Lambda function using node.js

exports.handler = async function (event, context) {
    console.log("Event", event)
    console.log("Context", context)
    return {
        message: "Hello from Node Lambda"
    }
}
