TARGETS := all clean package build
EXCLUDES := deploy/. results/.
SUBDIRS := $(filter-out $(EXCLUDES), $(wildcard */.))

$(TARGETS): $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TOPTARGETS) $(SUBDIRS)
