terraform {
  backend "http" {
  }
}

provider "aws" {
  region = "eu-central-1"
  default_tags {
    tags = {
      environment = "test"
      project = "lambda-runtimes-comparison"
      provisioner = "terraform"
    }
  }
}

locals {
  runtimes = ["python", "ruby", "nodejs", "golang", "java", "dotnet"] 
  codepath = "${path.module}/../.."
  config = [for runtime in local.runtimes : jsondecode(file("${local.codepath}/${runtime}/config.json"))]
}

resource "random_id" "id" {
  byte_length = 8
}

resource "aws_s3_bucket" "lambda-code-bucket" {
  bucket = "lambda-runtimes-${random_id.id.hex}"
}

data "aws_iam_policy_document" "lambda-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda-role" {
  name = "lambda-runtimes-role"
  assume_role_policy = data.aws_iam_policy_document.lambda-role-policy.json
}

data "aws_iam_policy_document" "lambda-policy" {
  statement {
    actions = ["logs:CreateLogStream", "logs:PutLogEvents"]
    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "lambda-policy" {
  name = "lambda-runtimes-policy"
  path = "/"
  description = "IAM Policy for managing AWS Lambda Roles"
  policy = data.aws_iam_policy_document.lambda-policy.json
}

resource "aws_iam_role_policy_attachment" "attach-policy-to-role" {
  role = aws_iam_role.lambda-role.name
  policy_arn = aws_iam_policy.lambda-policy.arn
}
 
resource "aws_s3_object" "lambda-code" {
  count = length(local.runtimes)
  bucket = aws_s3_bucket.lambda-code-bucket.bucket
  key = "${local.runtimes[count.index]}/code.zip"
  source = "${local.codepath}/${local.runtimes[count.index]}/code.zip"
  etag = filesha256("${local.codepath}/${local.runtimes[count.index]}/code.zip")
}

resource "aws_lambda_function" "lambda" {
  count = length(local.runtimes)
  s3_bucket = aws_s3_object.lambda-code[count.index].bucket
  s3_key = aws_s3_object.lambda-code[count.index].key
  source_code_hash = filesha256("${local.codepath}/${local.runtimes[count.index]}/code.zip")
  function_name = "${local.runtimes[count.index]}-lambda"
  role = aws_iam_role.lambda-role.arn
  handler = local.config[count.index].handler
  runtime = local.config[count.index].runtime
  memory_size = local.config[count.index].memory
  dynamic "environment" {
    for_each = local.config[count.index].environment
    content {
      variables = local.config[count.index].environment
    }
  }
  depends_on = [
    aws_iam_role_policy_attachment.attach-policy-to-role
  ]
}

resource "aws_cloudwatch_log_group" "lambda-log-group" {
  count = length(local.runtimes)
  name= "/aws/lambda/${aws_lambda_function.lambda[count.index].function_name}"
  retention_in_days = 5
}
