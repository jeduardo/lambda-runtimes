package main

import (
	"context"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

type InputEvent struct {
	Value string `json:"key"`
}

type ResponseEvent struct {
	Message string `json:"message"`
}

func HandleRequest(ctx context.Context, event InputEvent) (ResponseEvent, error) {
	log.Println("Context", ctx)
	log.Println("Event", event)
	return ResponseEvent{
		Message: "Hello from golang Lambda",
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
